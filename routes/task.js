const express = require('express');
const router = express.Router();
const Task = require('../models/task');
const User = require('../models/user');
const Status = require('../models/status');
const { ensureAuthenticated } = require('../config/auth')

router.get('/', (req, res) => {
  res.render('welcome');
})

router.get('/dashboard', ensureAuthenticated, async (req, res) => {
  const tasks = await Task.find();
  res.render('dashboard', { tasks });
});

router.get('/create_task', ensureAuthenticated, async (req, res) => {
  const users = await User.find();
  const statuses = await Status.find();
  res.render('taskCreate', { users, statuses });
});

router.get('/archive', ensureAuthenticated, async (req, res) => {
  const tasks = await Task.find();
  res.render('taskArchive', { tasks });
});

router.get('/edit/:id', ensureAuthenticated, async (req, res) => {
  const users = await User.find();
  const statuses = await Status.find();
  const task = await Task.findById(req.params.id);
  res.render('taskEdit', { task, users, statuses });
});

router.get('/archive_task/:id', ensureAuthenticated, async (req, res) => {
  let { id } = req.params;

  let archivedTask = await Task.findById({ _id: id });
  archivedTask.archive = !archivedTask.archive;
  await Task.updateOne({ _id: id }, archivedTask);
  res.redirect('/dashboard');
});

router.get('/delete/:id', ensureAuthenticated, async (req, res) => {
  let { id } = req.params;
  await Task.deleteOne({ _id: id });
  res.redirect('/dashboard');
});

router.post('/create_task', ensureAuthenticated, async (req, res) => {
  req.body.creator = req.user;

  const assignee = await User.findById(req.body.userId);
  req.body.assignee = assignee;

  const status = await Status.findById(req.body.status);
  if (status != null && status != undefined) {
    req.body.status = status;
  } else {
    const users = await User.find();
    req.body.status = new Status({ name: "NO STATUS", creator: users[0] });
  }

  const task = new Task(req.body);
  await task.save();

  res.redirect('/dashboard');
});

router.post('/edit/:id', ensureAuthenticated, async (req, res) => {
  const { id } = req.params;

  const assignee = await User.findById(req.body.userId);
  req.body.assignee = assignee;

  const status = await Status.findById(req.body.status);
  if (status != null && status != undefined) {
    req.body.status = status;
  } else {
    const users = await User.find();
    req.body.status = new Status({ name: "NO STATUS", creator: users[0] });
  }

  await Task.updateOne({ _id: id }, req.body);

  res.redirect('/dashboard');
});


module.exports = router;
