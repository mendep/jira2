const express = require('express');
const router = express.Router();
const User = require("../models/user");
const bcrypt = require('bcrypt');
const passport = require('passport');
const { ensureAuthenticated } = require('../config/auth')

router.get('/list_user', ensureAuthenticated, async (req, res) => {
    const users = await User.find();
    res.render('userList', { users });
});

router.get('/login', (req, res) => {
    res.render('login');
});

router.get('/register', (req, res) => {
    res.render('register')
});

router.get('/logout', (req, res) => {
    req.logout();
    req.flash('success_msg', 'Now logged out');
    res.redirect('/user/login');
})

router.get('/delete/:id', ensureAuthenticated, async (req, res) => {
    let { id } = req.params;
    await User.deleteOne({ _id: id });
    res.redirect('/user/list_user');
});

router.post('/login', (req, res, next) => {
    passport.authenticate('local', {
        successRedirect: '/dashboard',
        failureRedirect: '/user/login',
        failureFlash: true
    })(req, res, next)
});

router.post('/register', (req, res) => {
    const { name, email, password, password2 } = req.body;
    let errors = [];
    console.log(' Name ' + name + ' email :' + email + ' pass:' + password);
    
    // Handle errors in input
    if (!name || !email || !password || !password2) {
        errors.push({ msg: "Please fill in all fields" })
    }
    if (password !== password2) {
        errors.push({ msg: "passwords dont match" });
    }
    if (password.length < 6) {
        errors.push({ msg: 'password atleast 6 characters' })
    }
    if (errors.length > 0) {
        res.render('register', {
            errors: errors,
            name: name,
            email: email,
            password: password,
            password2: password2
        })
    } else {
        User.findOne({ email: email }).exec((err, user) => {
            console.log(user);
            if (user) {
                errors.push({ msg: 'email already registered' });
                res.render('register', { errors, name, email, password, password2 })
            } else {
                // Create user
                let newUser = new User({
                    name: name,
                    email: email,
                    password: password,
                    isAdmin: false
                });

                // Admin user
                if (email == 'mende.pashoski@outlook.com') {
                    newUser.isAdmin = true;
                }

                // Hash password
                bcrypt.genSalt(10, (err, salt) =>
                    bcrypt.hash(newUser.password, salt,
                        (err, hash) => {
                            if (err) throw err;

                            newUser.password = hash;
                            newUser.save()
                                .then((value) => {
                                    console.log(value)
                                    req.flash('success_msg', 'You have now registered!');
                                    res.redirect('/user/login');
                                })
                                .catch(value => console.log(value));
                        }));
            }
        })
    }
})

module.exports = router;