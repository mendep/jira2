const express = require('express');
const router = express.Router();
const Task = require('../models/task');
const User = require('../models/user');
const Status = require('../models/status');
const { ensureAuthenticated } = require('../config/auth')

router.get('/list_status', ensureAuthenticated, async (req, res) => {
    const statuses = await Status.find();
    res.render('statusList', { statuses });
});

router.get('/create_status', ensureAuthenticated, async (req, res) => {
    res.render('statusCreate');
});

router.get('/edit/:id', ensureAuthenticated, async (req, res) => {
    const status = await Status.findById(req.params.id);
    res.render('statusEdit', { status });
});

router.get('/delete/:id', ensureAuthenticated, async (req, res) => {
    let { id } = req.params;
    const statuses = await Status.find();
    let replacementStatus = statuses[0];

    // No status is left after this delete
    if (statuses.length == 1) {
        const users = await User.find();
        replacementStatus = new Status({ name: "NO STATUS", creator: users[0] });
        await replacementStatus.save();
    } else {
        // Choose a replacement status 
        for (let i = 0; i < statuses.length; i++) {
            if (statuses[i]._id == id) {
                // Take the previous if it exists, else the next one
                if (i > 0) {
                    replacementStatus = statuses[i - 1];
                    continue;
                } else if (i < statuses.length - 1) {
                    replacementStatus = statuses[i + 1];
                }
            }
        }
    }

    // Update tasks with replacement status
    const tasks = await Task.find();
    for (let i = 0; i < tasks.length; i++) {
        if (tasks[i].status._id == id) {
            tasks[i].status = replacementStatus;
            await Task.updateOne({ _id: tasks[i].id }, tasks[i]);
        }
    }

    await Status.deleteOne({ _id: id });
    res.redirect('/status/list_status');
});

router.post('/create_status', ensureAuthenticated, async (req, res) => {
    req.body.creator = req.user;
    const status = new Status(req.body);
    await status.save();
    res.redirect('/status/list_status');
});

router.post('/edit/:id', ensureAuthenticated, async (req, res) => {
    const { id } = req.params;
    await Status.updateOne({ _id: id }, req.body);
    res.redirect('/status/list_status');
});

module.exports = router;
