const express = require('express');
const mongoose = require('mongoose');
const app = express();
const expressEjsLayout = require('express-ejs-layouts')
const flash = require('connect-flash');
const session = require('express-session');
const passport = require("passport");
require('./config/passport')(passport)

// MongoDB connection
mongoose.connect('mongodb://localhost/jira', { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('db connected'))
  .catch((err) => console.log(err));

//Setup EJS
app.set('view engine', 'ejs');
app.use(expressEjsLayout);

// Setup Express
app.use(express.urlencoded({ extended: false }));
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use((req, res, next) => {
  res.locals.currentUser = req.user;
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
})

// Routes
app.use('/', require('./routes/task'));
app.use('/user', require('./routes/user'));
app.use('/status', require('./routes/status'));

// Start server
app.set('port', process.env.PORT || 3333);
app.listen(app.get('port'), () => {
  console.log(`server on port ${app.get('port')}`);
});