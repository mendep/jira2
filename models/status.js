const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    date: {
        type: Date,
        default: Date.now
    }
});

const StatusSchema = new Schema({
  name: {
      type: String,
      required: true
  },
  creator: {
      type: UserSchema,
      required: true
  }
});

module.exports = mongoose.model('Status', StatusSchema);
