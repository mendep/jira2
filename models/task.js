const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  date: {
    type: Date,
    default: Date.now
  }
});

const StatusSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  creator: {
    type: UserSchema,
    required: true
  }
});

const TaskSchema = Schema({
  title: {
    type: String,
    required: false
  },
  description: {
    type: String,
    required: false
  },
  assignee: {
    type: UserSchema,
    required: false
  },
  creator: {
    type: UserSchema,
    required: false
  },
  status: {
    type: StatusSchema,
    required: false
  },
  archive: {
    type: Boolean,
    default: false
  }
});

module.exports = mongoose.model('Task', TaskSchema);
